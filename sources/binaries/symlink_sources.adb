--  symlink_sources.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.IO_Exceptions;
with Ada.Directories;
with Ada.Text_IO;
use Ada.Text_IO;
with Personal_Ada;
use Personal_Ada;

with C_Imports;

procedure Symlink_Sources is
    procedure Create_Symlink (Path : String);
    --  Create a symlink, or recreate it, to the given path.

    procedure Create_Symlink (Path : String) is
        Dest_Path : constant String := Personal_Space_Path ("Ada/adainclude");

    begin
        Put_Line ("Symlinking path from """ & Path & """ to """
            & Dest_Path & """");

        begin
            --  Do not try to check if the file exists.  It is not a Typical
            --  file and Ada.Directories.Exists (Dest_Path) returns false when
            --  symlink do not exists or it exists.
            Put_Line ("Trying to delete symlink. Deleting " & Dest_Path);
            Ada.Directories.Delete_File (Dest_Path);

        exception
            when Ada.IO_Exceptions.Name_Error =>
                Put_Line ("No need to delete Symlink.");
        end;

        Put_Line ("Creating symlink...");
        C_Imports.Make_Symlink (Path, Dest_Path);
    end Create_Symlink;

begin
    Put_Line ("Searching for Adainclude");

    declare
        Adainclude_Path : constant String := Search_Adainclude_Path;
    begin
        if Adainclude_Path = "" then
            Put_Line
                ("The adainclude directory was not found on your system.");
            return;
        else
            Put_Line ("Found: " & Adainclude_Path);
        end if;

        Create_Personal_Space;

        Put_Line ("Recreating adainclude symbolic link.");
        Create_Symlink (Adainclude_Path);
    end;

end Symlink_Sources;
