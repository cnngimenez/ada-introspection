--  adainclue.ads ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

--
--  Personal Ada Space
--
--  A personal space is by defaut ~/Ada. It is where the Ada aplications is
--  installed. Usually, it can be installed at /usr/local but when the user has
--  no root privileges it may choose this personal Space.
--
--  It is preferable that the space has a symbolic link to the adainclude
--  directory. This is where GNAT/GCC search for ads and adb files Ada Default
--  libraries.
package Personal_Ada is

    function Personal_Space_Path (Subpath : String := "Ada") return String;
    --  Return the personal space full ptah

    procedure Create_Personal_Space (Path : String := "Ada");
    --  Create diretories for the personal space in your home directory.

    function Search_Adainclude_Path (From_Path : String := "/usr/lib/gcc";
                                     Max_Level : Natural := 3)
        return String;
    --  Search where Adainclude is installed in your system.

    procedure Create_Adainclude_Symlink (Where_Path : String := "Ada");
    --  Create a symlink on ~/Ada/Adainclude.

end Personal_Ada;
