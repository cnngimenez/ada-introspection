--  c_imports.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Interfaces.C;
--  use Interfaces.C;

package body C_Imports is

    package C renames Interfaces.C;
    use C;

    procedure Symlink (Name1 : char_array; Name2 : char_array) with
        Import => True,
        Convention => C,
        External_Name => "symlink";

    procedure Make_Symlink (Target : String; Linkpath : String) is
    begin
        Symlink (To_C (Target), To_C (Linkpath));
    end Make_Symlink;

end C_Imports;
