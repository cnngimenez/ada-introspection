--  personal_ada.adb ---

--  Copyright 2022 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/Licenses/>.

-------------------------------------------------------------------------

with Ada.IO_Exceptions;
with Ada.Environment_Variables;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Directories;
use Ada.Directories;

with C_Imports;

package body Personal_Ada is

    procedure Create_Adainclude_Symlink (Where_Path : String := "Ada") is
        Adainclude_Path : constant String := Search_Adainclude_Path;
        Dest_Path : constant String :=
            Personal_Space_Path (Where_Path & "/adainclude");
    begin
        Create_Personal_Space (Where_Path);

        begin
            --  Do not try to check if the file exists.  It is not a typical
            --  file and Ada.Directories.Exists (Dest_Path) returns false when
            --  symlink do not exists or it exists.
            Ada.Directories.Delete_File (Dest_Path);
        exception
            when Ada.IO_Exceptions.Name_Error =>
                --  Put_Line ("No need to delete Symlink.");
                null;
        end;

        C_Imports.Make_Symlink (Adainclude_Path, Dest_Path);
    end Create_Adainclude_Symlink;

    procedure Create_Personal_Space (Path : String := "Ada") is
        Ada_Path : constant String := Personal_Space_Path (Path);
    begin
        if not Ada.Directories.Exists (Ada_Path) then
            Ada.Directories.Create_Directory (Ada_Path);
        end if;
    end Create_Personal_Space;

    function Personal_Space_Path (Subpath : String := "Ada") return String is
        Home_Path : constant String :=
            Ada.Environment_Variables.Value ("HOME", "");
    begin
        return Home_Path & "/" & Subpath;
    end Personal_Space_Path;

    function Search_Adainclude_Path (From_Path : String := "/usr/lib/gcc";
                                     Max_Level : Natural := 3)
        return String
    is
        procedure Recursive_Search (Directory_Entry : Directory_Entry_Type);

        Dir_Filter : constant Filter_Type := (
            Directory => True,
            Ordinary_File => False,
            Special_File => False
            );

        Found : Boolean := False;
        Level : Natural := 0;
        Out_Path : Unbounded_String;

        procedure Recursive_Search (Directory_Entry : Directory_Entry_Type) is
            Dirname : constant String := Simple_Name (Directory_Entry);
            Path : constant String := Full_Name (Directory_Entry);
        begin
            if Found or else Level >= Max_Level then
                return;
            end if;

            if Dirname = "." or else Dirname = ".." then
                return;
            end if;

            if Dirname = "adainclude" then
                Out_Path := To_Unbounded_String (Path);
                Found := True;
                return;
            end if;

            Level := Level + 1;
            --  Put_Line ("Entering level " & Level'Image & " - " & Path);
            Search (Path, "", Dir_Filter, Recursive_Search'Access);
            Level := Level - 1;
        end Recursive_Search;

    begin
        Search (From_Path, "", Dir_Filter, Recursive_Search'Access);
        return To_String (Out_Path);
    end Search_Adainclude_Path;

end Personal_Ada;
